package com.mohstats.mohwf;

import java.text.DecimalFormat;

import org.json.JSONException;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.LoaderSettings;
import com.novoda.imageloader.core.LoaderSettings.SettingsBuilder;
import com.novoda.imageloader.core.loader.Loader;
import com.novoda.imageloader.core.model.ImageTag;
import com.novoda.imageloader.core.model.ImageTagFactory;

public class PageFragment extends Fragment{

	public static PageFragment newInstance(int page) {
		  
        PageFragment pageFragment = new PageFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("key", page);
        pageFragment.setArguments(bundle);
        return pageFragment;
    }

	private ImageManager imageManager;
	private Context context;
	private ImageTagFactory imageTagFactory;
	private Loader imageLoader;
	private String tag = "djevtic";
	public Player player = Player.getInstance();
	private ImageView bfstatimage;
	private ImageView oldguys;
	@Override  
    public void onCreate(Bundle savedInstanceState) {  
        super.onCreate(savedInstanceState);  
    }  

	@Override  
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {  
        int page = getArguments().getInt("key");
        View view = null;
        LoadImageLibs();
        if(page ==0){
        	view = inflater.inflate(R.layout.playerstatlayout, container, false);
        	fillPage(page,view);
        }else if(page==6){
        	view = inflater.inflate(R.layout.about, container, false);
        	fillPage(page,view);
            addListenerOnButton(view);
            addImageOnClickListener(view);
        }
        return view;  
    }
	
	private void fillPage(int page, View view) {
		if(page ==0){
		TextView playerName = (TextView) view.findViewById(R.id.PlayerName);
		TextView playerCountry = (TextView) view.findViewById(R.id.textCountry);
		TextView playerKillStreak = (TextView) view.findViewById(R.id.textKillStreak);
		TextView playerShots = (TextView) view.findViewById(R.id.textShots);
		TextView playerHits = (TextView) view.findViewById(R.id.textHits);
		TextView playerAcuracy = (TextView) view.findViewById(R.id.textAccuracy);
		TextView playerScore = (TextView) view.findViewById(R.id.textScore);
		TextView playerTime = (TextView) view.findViewById(R.id.textTime);
		TextView playerSMP = (TextView) view.findViewById(R.id.textSPM);
		TextView playerKD = (TextView) view.findViewById(R.id.textKD);
		TextView playerWin = (TextView) view.findViewById(R.id.textWin);
		TextView playerLoss = (TextView) view.findViewById(R.id.textLosses);
		TextView playerWL = (TextView) view.findViewById(R.id.textWL);
		TextView playerKills = (TextView) view.findViewById(R.id.textKils);
		TextView playerDeatsh = (TextView) view.findViewById(R.id.textDeaths);
		TextView playerWinStreak = (TextView) view.findViewById(R.id.textWinStreak);
		TextView playerTokenSpent = (TextView) view.findViewById(R.id.textTokenSpent);
		TextView playerTokenLeft = (TextView) view.findViewById(R.id.textTokenLeft);
		TextView playerMelekils = (TextView) view.findViewById(R.id.textMelleKils);
		TextView playerFirst = (TextView) view.findViewById(R.id.textFirst);
		TextView playerSecound = (TextView) view.findViewById(R.id.textSecond);
		TextView playerThird = (TextView) view.findViewById(R.id.textThird);
		TextView playerTop3 = (TextView) view.findViewById(R.id.textTop3);

		playerName.setText(player.getPlayerName());
		try {
			playerCountry.setText(player.getJSON().getString("country_name"));
			playerKillStreak.setText(player.getJSON().getJSONObject("stats").getJSONObject("global").getString("killstreak"));
			playerShots.setText(player.getJSON().getJSONObject("stats").getJSONObject("global").getString("shots"));
			playerHits.setText(player.getJSON().getJSONObject("stats").getJSONObject("global").getString("hits"));
			playerAcuracy.setText(""
					+ GetAccuracy(player.getJSON().getJSONObject("stats").getJSONObject("global").getInt("hits"),
							player.getJSON().getJSONObject("stats").getJSONObject("global").getInt("shots")) + "%");
			playerScore.setText(player.getJSON().getJSONObject("stats").getJSONObject("scores").getString("score"));
			playerTime.setText("" + getTime(player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("time")));
			playerSMP.setText(""
					+ getScorePerMinute(player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("score"),
							player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("time")));
			playerKD.setText(""	+ GetTwoDecimal(player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("kills"),player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("deaths")));
			playerWin.setText(""+player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("wins"));
			playerLoss.setText(""+player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("losses"));
			playerWL.setText(""+ GetTwoDecimal(player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("wins"),player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("losses")));
			playerKills.setText("" + player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("kills"));
			playerDeatsh.setText("" + player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("deaths"));
			playerWinStreak.setText("" + player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("win_streak"));
			playerTokenSpent.setText("" + player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("tokens_spent"));
			playerTokenLeft.setText("" + player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("tokens_left"));
			playerMelekils.setText("" + player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("meleekills"));
			playerFirst.setText("" + player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("first"));
			playerSecound.setText("" + player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("second"));
			playerThird.setText("" + player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("third"));
			playerTop3.setText("" + player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("top3"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}

		
	}

	private void LoadImageLibs() {
		LoaderSettings settings = new SettingsBuilder().withDisconnectOnEveryCall(true).build(getActivity());
		imageManager = new ImageManager(getActivity(), settings);
		imageTagFactory = new ImageTagFactory(getActivity(),R.drawable.oldguysgamingscaled);
		imageTagFactory.setErrorImageId(R.drawable.oldguysgamingscaled);
		imageLoader = imageManager.getLoader();
		
	}
    
    private void setImage(ImageView imageView, String url) {
		Log.v(tag  ,"ImageView == "+imageView);
		Log.v(tag ,"url == "+url);
		String link = "http://dl.dropbox.com/u/1480498/bf3/" + url;
		ImageTag tag = imageTagFactory.build(link);
		imageView.setTag(tag);
		imageLoader.load(imageView);
	}
    
    public String getTime(long timeLong){
		String timeString = "";
		int seconds = (int) (timeLong) % 60 ;
		int minutes = (int) ((timeLong/60) % 60);
		int hours   = (int) ((timeLong / (60*60)) % 24);
		int days   = (int) ((timeLong / (60*60*24)) % 365);
		if(days>0){
			timeString = timeString+days+"d ";
		}
		if(hours>0){
			timeString = timeString+hours+"h ";
		}
		timeString = timeString+minutes+"m "+seconds+"s ";
		return timeString;
	}
	
	public String GetAccuracy(long first, long secound){
		 
        double d;
        d = ((double)first/(double)secound)*100;
        DecimalFormat f = new DecimalFormat("#0.00");  // this will helps you to always keeps in two decimal places
        return f.format(d); 
	}
	
	private long getScorePerMinute(long playerScore, long playerGameTime) {
		long gameTimeInMinutes = playerGameTime/60;
		long scorePerMinute = playerScore/gameTimeInMinutes;
		return scorePerMinute;
	}
	
	public String GetTwoDecimal(long first, long secound){
		 
        double d;
        d = (double)first/(double)secound;
        DecimalFormat f = new DecimalFormat("#0.00");  // this will helps you to always keeps in two decimal places
        return f.format(d); 
	}
	
	private void addImageOnClickListener(View view) {
		bfstatimage = (ImageView) view.findViewById(R.id.bfstatimage);
		bfstatimage.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent urlIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://bf3stats.com/"));
				startActivity(urlIntent);
			}
 
		});
		oldguys = (ImageView) view.findViewById(R.id.bfstatimage);
		oldguys.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent urlIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.oldguys.eu/"));
				startActivity(urlIntent);
			}
 
		});
	}
	
	public void addListenerOnButton(View view) {
		 
		Button emailButton = (Button) view.findViewById(R.id.email_button);
		Button marketButton = (Button) view.findViewById(R.id.market_button);
 
		emailButton.setOnClickListener(new OnClickListener() {
 
			@Override
			public void onClick(View arg0) {
 
				Intent i = new Intent(Intent.ACTION_SEND);
				i.setType("message/rfc822");
				i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"gisseares@gmail.com"});
				i.putExtra(Intent.EXTRA_SUBJECT, "BF3Stats");
				i.putExtra(Intent.EXTRA_TEXT   , "Enter your text here");
				try {
				    startActivity(Intent.createChooser(i, "Send mail..."));
				} catch (android.content.ActivityNotFoundException ex) {
				    Toast.makeText(getActivity(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
				}
 
			}
 
		});
		
		marketButton.setOnClickListener(new OnClickListener() {
			 
			@Override
			public void onClick(View arg0) {
 
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("market://details?id=com.bf3stats"));
				startActivity(intent);
 
			}
 
		});
 
	}
}
