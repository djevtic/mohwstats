package com.mohstats.mohwf;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;

public class DeletePlayer extends Activity{
	private Context context;
	private Database db;
	private Cursor c;
	private Button button;
	private CheckBox[] cbs;
	private String tag = "djevtic";
	int numberOfPlayers = 0;

	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();
        setContentView(R.layout.deleteplayerlayout);
        initializeComponents();
        addListenerOnButton();
    }

	private void addListenerOnButton() {
			 
			button = (Button) findViewById(R.id.deletePlayerButton);
	 
			button.setOnClickListener(new OnClickListener() {
	 
				private int playerselectedID;

				@Override
				public void onClick(View arg0) {
					if(numberOfPlayers>0){
						db.open();
					}
					for(int i = 0; numberOfPlayers>i;i++){
						if(cbs[i].isChecked()){
							playerselectedID = cbs[i].getId();
							if(db.deletePlayer((long)playerselectedID)){
							}
						}
					}
					if(numberOfPlayers>0){
						db.close();
					}
					Intent main = new Intent(context, PlayerSelector.class);
					DeletePlayer.this.startActivity(main);
				}	 
			});	 				
	}

	private void initializeComponents() {
		LinearLayout ll = (LinearLayout)findViewById(R.id.deleteplayerlayoutinternal);
		db = new Database(this);
		db.open();
		c = db.getAllPlayers();
		if (c != null ) {
			numberOfPlayers = 0;
			if  (c.moveToFirst()) {
		        do {
		            numberOfPlayers++;
		        }while (c.moveToNext());
		    }cbs = new CheckBox[numberOfPlayers];
			if  (c.moveToFirst()) {
				int i = 0;
		        do {
		        	cbs[i]  = new CheckBox(this);
		        	cbs[i].setText(c.getString(c.getColumnIndex("name")));
		        	cbs[i].setId(Integer.parseInt(c.getString(c.getColumnIndex("id"))));
		        	ll.addView(cbs[i]);
				    i++;
		        }while (c.moveToNext());
		    }
			String[] playerIds = new String[numberOfPlayers];			
			c.close();
			db.close();
		}
		
	}
	@Override
    public void onBackPressed(){
		Intent main = new Intent(context, PlayerSelector.class);
		DeletePlayer.this.startActivity(main);
	}
}
