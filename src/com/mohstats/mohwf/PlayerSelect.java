package com.mohstats.mohwf;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Toast;

public class PlayerSelect extends Activity{

	private String tag = "djevtic";

	private static final int PROGRESS = 0x1;

	private ProgressBar mProgress;
	private int mProgressStatus = 0;
	Runnable runnable = null;
	Context context = null;

	private Button button;
	private ImageView image;

	private Database db;
	private Cursor c;
	private boolean thereArePlayers = false;
	private boolean keyIsPressent = false;

	private String sicretKey = "Chx9rPGf0WmRoALPn479EKhywRm8Kou1";
	private String appidentification = "A9AZqQungp";

	private StringBuffer something;

		/** Called when the activity is first created. */
	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.playerselectlayout);
	        context = getApplicationContext();
	        Bundle extras = getIntent().getExtras();
	        if (extras != null) {
	        	boolean value1 = extras.getBoolean("error");
	            if (value1 != true) {
	            	showErrorMessage("We couldn't found player on that specific platform or MOHWStats are down at moment");
	            }
	        }  
	        addListenerOnButton();
	        addImageOnClickListener();
	        isOnline();
	    }
	 
		
		private void addImageOnClickListener() {
			image = (ImageView) findViewById(R.id.imageView1);
			image.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					Intent urlIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.oldguys.eu"));
					startActivity(urlIntent);
				}
	 
			});
		}

		public void addListenerOnButton() {
	    	 
			button = (Button) findViewById(R.id.confirmButton);
	 
			button.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					String selectedPlatform = getRadioButon();
					String selectedPlayername = getPlayername();
					if(selectedPlayername!=""&&selectedPlayername!=null&&isOnline()){
						Intent i = new Intent(context, LoadingData.class);
						i.putExtra("name", selectedPlayername);
						i.putExtra("platform", selectedPlatform);
						startActivity(i);
					}else{
						showErrorMessage("Please fill Player Name and select Platform for which you want statistic to be shown.");
					}
				}
	 
			});
	 
		}

		protected void showErrorMessage(CharSequence connectionErrorMessage) {
			Context context = getApplicationContext();
	        int duration = Toast.LENGTH_LONG;
	        Toast toast = Toast.makeText(context, connectionErrorMessage, duration);
	    	toast.show();
		}

		protected String  getPlayername() { 
			String playerName = "";
			EditText editText = (EditText) findViewById(R.id.editPlayerName);
			playerName = editText.getText().toString();
			editText.length();
			if(editText.length()==0){
				playerName = "";
			}
			return playerName;
		}

		protected String getRadioButon() {
			RadioGroup radioGroup = (RadioGroup) findViewById(R.id.platform);
			int checkedRadioButton = radioGroup.getCheckedRadioButtonId();
			 
			String radioButtonSelected = "";
			 
			if (checkedRadioButton == R.id.pc) {
				radioButtonSelected = "pc";
			} else if (checkedRadioButton == R.id.ps3) {
				radioButtonSelected = "ps3";
			} else if (checkedRadioButton == R.id.xbox) {
				radioButtonSelected = "360";
			}
			return radioButtonSelected;
		}
		
		public boolean isOnline() {
		    ConnectivityManager cm =
		        (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

		    boolean connection = cm.getActiveNetworkInfo() != null && 
		       cm.getActiveNetworkInfo().isConnectedOrConnecting();
		    if(!connection){
		    	showErrorMessage("For this to work you need internet connection.");
		    }
		    return connection;
		    
		}
		
		@Override
	    public void onBackPressed(){
			db = new Database(this);
			db.open();
			c = db.getAllPlayers();
			if (c.moveToFirst()) {
				thereArePlayers = true;
			}
			c.close();
			db.close();
			if(thereArePlayers){
				Intent playerGeneralStats = new Intent(PlayerSelect.this, PlayerSelector.class);
	            PlayerSelect.this.startActivity(playerGeneralStats);
			}else{
				Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		}
}
