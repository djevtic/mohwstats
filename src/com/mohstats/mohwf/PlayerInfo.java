package com.mohstats.mohwf;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;

import org.json.JSONException;
import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.LoaderSettings;
import com.novoda.imageloader.core.LoaderSettings.SettingsBuilder;
import com.novoda.imageloader.core.loader.Loader;
import com.novoda.imageloader.core.model.ImageTag;
import com.novoda.imageloader.core.model.ImageTagFactory;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

public class PlayerInfo extends Activity {
	private Player player;
	private Context context;
	private Database db;
	private String tag = "djevtic";
	private Cursor c;
	private String playerID;
	private boolean idweupdated;
	private ImageManager imageManager;
	private ImageTagFactory imageTagFactory;
	private Loader imageLoader;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.playerstatlayout);
		context = getApplicationContext();
		player = Player.getInstance();
		if (player.getPlayerName() != null) {
			databaseCheck();
			fillPage();
		} else {
			Intent i = new Intent(context, PlayerSelector.class);
			startActivity(i);
		}
		// Odavde poceto sa image JAR implementacijom
		LoaderSettings settings = new SettingsBuilder()
				.withDisconnectOnEveryCall(true).build(context);
		imageManager = new ImageManager(context, settings);
		imageTagFactory = new ImageTagFactory(context,
				R.drawable.oldguysscaledmenu);
		imageTagFactory.setErrorImageId(R.drawable.oldguysscaledmenu);
		imageLoader = imageManager.getLoader();
		// Ovde je kraj
	}

	private void databaseCheck() {
		boolean playerExist = false;
		db = new Database(this);
		db.open();
		long id = 0;
		c = db.getAllPlayers();
		if (c != null) {
			if (c.moveToFirst()) {
				do {
					if (player.getPlayerName().equals(
							c.getString(c.getColumnIndex("name")))) {
						playerExist = true;
						playerID = c.getString(c.getColumnIndex("id"));
					}
				} while (c.moveToNext());
			}
		}
		if (!playerExist) {
			id = db.insertPlayer(
					player.getPlayerName(),
					player.getPlayerPlatform(),
					player.getPlayerRankImmage(),
					player.getPlayerRank(),
					""
							+player.getPlayerScore(),
					""
							+ ((player.getPlayerNextRankScore() - player
									.getPlayerPreviouseRankScore()) / 2), "0");
		} else {
			idweupdated = db.updatePlayer(
					Long.parseLong(playerID),
					player.getPlayerName(),
					player.getPlayerPlatform(),
					player.getPlayerRankImmage(),
					player.getPlayerRank(),
					""
							+ player.getPlayerScore(),
					""
							+ ((player.getPlayerNextRankScore() - player
									.getPlayerPreviouseRankScore()) / 2), "0");
		}
		db.close();

	}

	private void fillPage() {
		TextView playerName = (TextView) findViewById(R.id.PlayerName);
		//ImageView playerRankImg = (ImageView) findViewById(R.id.playerRankImg);
		TextView playerCountry = (TextView) findViewById(R.id.textCountry);
		TextView playerKillStreak = (TextView) findViewById(R.id.textKillStreak);
		TextView playerShots = (TextView) findViewById(R.id.textShots);
		TextView playerHits = (TextView) findViewById(R.id.textHits);
		TextView playerAcuracy = (TextView) findViewById(R.id.textAccuracy);
		TextView playerScore = (TextView) findViewById(R.id.textScore);
		TextView playerTime = (TextView) findViewById(R.id.textTime);
		TextView playerSMP = (TextView) findViewById(R.id.textSPM);
		TextView playerKD = (TextView) findViewById(R.id.textKD);
		TextView playerWin = (TextView) findViewById(R.id.textWin);
		TextView playerLoss = (TextView) findViewById(R.id.textLosses);
		TextView playerWL = (TextView) findViewById(R.id.textWL);
		TextView playerKills = (TextView) findViewById(R.id.textKils);
		TextView playerDeatsh = (TextView) findViewById(R.id.textDeaths);
		TextView playerWinStreak = (TextView) findViewById(R.id.textWinStreak);
		TextView playerTokenSpent = (TextView) findViewById(R.id.textTokenSpent);
		TextView playerTokenLeft = (TextView) findViewById(R.id.textTokenLeft);
		TextView playerMelekils = (TextView) findViewById(R.id.textMelleKils);
		TextView playerFirst = (TextView) findViewById(R.id.textFirst);
		TextView playerSecound = (TextView) findViewById(R.id.textSecond);
		TextView playerThird = (TextView) findViewById(R.id.textThird);
		TextView playerTop3 = (TextView) findViewById(R.id.textTop3);

		playerName.setText(player.getPlayerName());
		try {
			playerCountry.setText(player.getJSON().getString("country_name"));
			//setImage(playerRankImg, player.getPlayerRankImmage());
			playerKillStreak.setText(player.getJSON().getJSONObject("stats").getJSONObject("global").getString("killstreak"));
			playerShots.setText(player.getJSON().getJSONObject("stats").getJSONObject("global").getString("shots"));
			playerHits.setText(player.getJSON().getJSONObject("stats").getJSONObject("global").getString("hits"));
			playerAcuracy.setText(""
					+ GetAccuracy(player.getJSON().getJSONObject("stats").getJSONObject("global").getInt("hits"),
							player.getJSON().getJSONObject("stats").getJSONObject("global").getInt("shots")) + "%");
			playerScore.setText(player.getJSON().getJSONObject("stats").getJSONObject("scores").getString("score"));
			playerTime.setText("" + getTime(player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("time")));
			playerSMP.setText(""
					+ getScorePerMinute(player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("score"),
							player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("time")));
			playerKD.setText(""	+ GetTwoDecimal(player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("kills"),player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("deaths")));
			playerWin.setText(""+player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("wins"));
			playerLoss.setText(""+player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("losses"));
			playerWL.setText(""+ GetTwoDecimal(player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("wins"),player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("losses")));
			playerKills.setText("" + player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("kills"));
			playerDeatsh.setText("" + player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("deaths"));
			playerWinStreak.setText("" + player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("win_streak"));
			playerTokenSpent.setText("" + player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("tokens_spent"));
			playerTokenLeft.setText("" + player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("tokens_left"));
			playerMelekils.setText("" + player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("meleekills"));
			playerFirst.setText("" + player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("first"));
			playerSecound.setText("" + player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("second"));
			playerThird.setText("" + player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("third"));
			playerTop3.setText("" + player.getJSON().getJSONObject("stats").getJSONObject("global").getLong("top3"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public String getTime(long timeLong) {
		String timeString = "";
		int seconds = (int) (timeLong) % 60;
		int minutes = (int) ((timeLong / 60) % 60);
		int hours = (int) ((timeLong / (60 * 60)) % 24);
		int days = (int) ((timeLong / (60 * 60 * 24)) % 365);
		if (days > 0) {
			timeString = timeString + days + "d ";
		}
		if (hours > 0) {
			timeString = timeString + hours + "h ";
		}
		timeString = timeString + minutes + "m " + seconds + "s ";
		return timeString;
	}

	public String GetAccuracy(long first, long secound) {

		double d;
		d = ((double) first / (double) secound) * 100;
		DecimalFormat f = new DecimalFormat("#0.00"); // this will helps you to
														// always keeps in two
														// decimal places
		return f.format(d);
	}

	private long getScorePerMinute(long playerScore, long playerGameTime) {
		if (playerScore == 0 || playerGameTime == 0) {
			return 0;
		}
		long gameTimeInMinutes = playerGameTime / 60;
		long scorePerMinute = playerScore / gameTimeInMinutes;
		return scorePerMinute;
	}

	public String GetTwoDecimal(long first, long secound) {

		double d;
		d = (double) first / (double) secound;
		DecimalFormat f = new DecimalFormat("#0.00"); // this will helps you to
														// always keeps in two
														// decimal places
		return f.format(d);
	}

	@Override
	public void onBackPressed() {
		Intent i = new Intent(context, PlayerSelector.class);
		startActivity(i);
	}

	private void setImage(ImageView imageView, String url) {
		String link = "http://dl.dropbox.com/u/1480498/mw/" + url;
		ImageTag tag = imageTagFactory.build(link);
		imageView.setTag(tag);
		imageLoader.load(imageView);
	}

}
