package com.mohstats.mohwf;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Application;
import android.content.Context;
import android.database.Cursor;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;

public class Player extends Application {

	private static Player singleton;
	/*
	 * Utility stuff
	 */
	private String tag = "djevtic";
	private boolean connectionprobelem = false;
	public boolean weAreGood = true;
	private String sendURL = "";
	Runnable runnable = null;
	HashMap<String, Integer> meMap;
	String extStorageDirectory;
	private static String TAG = "MyMainApplication";
	private int count = 0;
	private Context context;
	InputStream content = null;
	HttpEntity entity = null;
	StatusLine statusLine = null;
	private JSONArray nextRank;
	private JSONObject row;
	private String sicretKey = "hG1iMt9TxO5XE4an6knt9PWDwGo22DlE";
	private String appidentification = "A9AZqQungp";
	private Database db;
	private Cursor c = null;
	private boolean keyIsPressent;
	private StringBuffer something;
	private JSONObject jsonKey;
	private String jsonKeyString;
	private String status = "error";
	private String localIdent;
	private String localKey;
	private String player2name;
	private String player2platform;
	private HttpPost post;
	private int state;
	private String playerPlatform;
	private String url = "http://api.mohwstats.com/";
	private String playerName;
	private String jsonString;
	private String jsonString2;
	private JSONObject jsonObject2;
	private JSONObject jsonObject;
	private String TAG_DOGTAGS;
	private JSONObject dogtags;
	private Object rankAndScore;
	private JSONObject statsJson;
	private JSONObject weaponJson;
	private String rank;
	private String rankimage;
	private int score;
	private int previouseScore;
	private int nextRankScore;
	private String playerFlag;
	private String playerCountry;
	private String playerKillStreak;
	private int playerShootsFired;
	private int playerHits;
	private long playerGameTime;
	private long playerKills;
	private long playerDeaths;
	private long playerGameWin;
	private long playerGameLose;
	private String playerWinStreak;
	private int playerTokenSpent;
	private int playerTokenLeft;
	private int playerMelleKils;
	private int playerFirstPlace;
	private int playerSecondPlace;
	private int playerThirdPlace;
	private int playerTop3;
	private JSONObject nationsJson;
	private JSONObject kitJson;
	private JSONObject ribbonJson;
	private JSONObject medalsJson;
	private JSONObject answerJSON;
	private String code;
	private StringBuffer answer;
	private boolean updateproblem;
	private boolean addProblems;
	private StringBuffer addPlayerAnswer;
	public boolean continueWithExecution;
	private boolean result;

	@Override
	public void onCreate() {
		super.onCreate();
		singleton = this;
	}

	public static Player getInstance() {
		return singleton;
	}

	public static long now() {
		return toUnixTime(Calendar.getInstance().getTime());
	}

	public static long toUnixTime(Date paramDate) {
		return paramDate.getTime() / 1000L;
	}

	public void GetPlayer(final String playername, final String platform) {
		setState(0);
		setPlayerName(playername);
		setPlayerPlatform(platform);
		context = getApplicationContext();
		continueWithExecution = false;
		weAreGood = false;
		runnable = new Runnable() {
			public void run() {
				CheckAppKey();
				UpdatePlayer(playername,platform, localIdent, "" + now());
				if (!getUpdateProblem()) {
					ConnectingToStats(1);
				}else {
					AddPlayerToMohstats(playername,platform, localIdent, "" + now());
					if (!GetAddProblem()) {
						ConnectingToStats(1);
					} else {
						setState(1);
					}
				}
			}
		};
		new Thread(runnable).start();
		continueWithExecution = true;
	}

	protected boolean GetAddProblem() {
		return addProblems;
	}

	public int GetPlayers(final String playername1, final String platform1,
			final String playername2, final String platform2) {
		setState(0);
		setPlayerName(playername1);
		setPlayerPlatform(platform1);
		setPlayer2Name(playername2);
		setPlayer2Platform(platform2);
		context = getApplicationContext();
		weAreGood = false;
		runnable = new Runnable() {
			public void run() {
				CheckAppKey();
				UpdatePlayer(playername1,platform1, localIdent, "" + now());
				if (!getUpdateProblem()) {
					ConnectingToStats(1);
				} else {
					setState(1);
				}
				StringBuffer updatePlayer2 = UpdatePlayer2(playername2,
						platform2, localIdent, "" + now());
				if (updatePlayer2 != null || updatePlayer2.equals("ERROR")) {
					ConnectingToStats(2);
				} else {
					setState(1);
				}
			}
		};
		new Thread(runnable).start();
		return this.state;
	}

	protected boolean getUpdateProblem() {
		return updateproblem;
	}

	protected void UpdatePlayer(String playerName, String platform,
			String appIdent, String timestamp) {

		// Build parameter string
		// String data =
		// "time="+timestamp+"&ident="+appIdent+"&player="+playerName;
		String data;
		String sig;
		setUpdateProblem(true);

		try {

			// Send the request
			URL url = new URL("http://api.mohwstats.com/" + platform
					+ "/playerupdate/");
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			OutputStreamWriter writer = new OutputStreamWriter(
					conn.getOutputStream());

			// JSON and Base64 encoding for "data" string
			JSONObject json = new JSONObject();
			try {
				json.put("time", timestamp);
				json.put("ident", appIdent);
				json.put("player", playerName);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			data = json.toString();
			String encoded64data = str_replace(Base64.encodeToString(
					data.getBytes(), 10));

			// SHA256-HMAC and Base64 encoding for "sig" string

			String encoded64sig = null;
			try {
				SecretKey signingKey = new SecretKeySpec(localKey.getBytes(),
						"HMACSHA256");
				Mac mac = Mac.getInstance("HMACSHA256");
				mac.init(signingKey);
				byte[] digest = mac.doFinal(encoded64data.getBytes()); 
				encoded64sig = str_replace(Base64.encodeToString(digest, 10)); 
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (InvalidKeyException e) {
				e.printStackTrace();
			}
			// write parameters
			writer.write("data=" + encoded64data + "&sig=" + encoded64sig);
			writer.flush();

			// Get the response
			answer = new StringBuffer();
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					conn.getInputStream()));
			String line;
			while ((line = reader.readLine()) != null) {
				answer.append(line);
			}
			writer.close();
			reader.close();
			try {
				answerJSON = new JSONObject(answer.toString());
				Log.v(tag,"Player.updatePlayer: "+answerJSON.toString());
				result = answerJSON.getJSONObject("task").isNull("result");
				//if(!result){
					code = answerJSON.getString("cmd");
				//}
			} catch (JSONException e) {
				setUpdateProblem(false);
			}

		} catch (MalformedURLException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}

	private void setUpdateProblem(boolean b) {
		this.updateproblem = b;
		
	}

	private void setAnswer(StringBuffer answer2) {
		this.answer = answer2;
		
	}

	protected StringBuffer UpdatePlayer2(String playerName, String platform,
			String appIdent, String timestamp) {
		// Build parameter string
		// String data =
		// "time="+timestamp+"&ident="+appIdent+"&player="+playerName;
		String data;
		String sig;

		try {

			// Send the request
			URL url = new URL("http://api.mohwstats.com/" + platform
					+ "/playerupdate/");
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			OutputStreamWriter writer = new OutputStreamWriter(
					conn.getOutputStream());

			// JSON and Base64 encoding for "data" string
			JSONObject json = new JSONObject();
			try {
				json.put("time", timestamp);
				json.put("ident", appIdent);
				json.put("player", playerName);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			data = json.toString();
			String encoded64data = str_replace(Base64.encodeToString(
					data.getBytes(), 10));

			// SHA256-HMAC and Base64 encoding for "sig" string

			String encoded64sig = null;
			try {
				SecretKey signingKey = new SecretKeySpec(localKey.getBytes(),
						"HMACSHA256");
				Mac mac = Mac.getInstance("HMACSHA256");
				mac.init(signingKey);
				byte[] digest = mac.doFinal(encoded64data.getBytes()); // output
																		// of
																		// sha256
				encoded64sig = str_replace(Base64.encodeToString(digest, 10)); // using
																				// it
																				// as
																				// an
																				// input
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (InvalidKeyException e) {
				e.printStackTrace();
			}

			// write parameters
			writer.write("data=" + encoded64data + "&sig=" + encoded64sig);
			writer.flush();

			// Get the response
			StringBuffer answer = new StringBuffer();
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					conn.getInputStream()));
			String line;
			while ((line = reader.readLine()) != null) {
				answer.append(line);
			}
			writer.close();
			reader.close();

			// Return the response
			return answer;

		} catch (MalformedURLException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		return null;
	}

	protected void AddPlayerToMohstats(String playerName,
			String platform, String appIdent, String timestamp) {

		// Build parameter string
		// String data =
		// "time="+timestamp+"&ident="+appIdent+"&player="+playerName;
		String data;
		String sig;
		setAddProblems(true);
		try {

			// Send the request
			URL url = new URL("http://api.mohwstats.com/" + platform
					+ "/playerlookup/");
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			OutputStreamWriter writer = new OutputStreamWriter(
					conn.getOutputStream());

			// JSON and Base64 encoding for "data" string
			JSONObject json = new JSONObject();
			try {
				json.put("time", timestamp);
				json.put("ident", appIdent);
				json.put("player", playerName);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			data = json.toString();
			String encoded64data = str_replace(Base64.encodeToString(
					data.getBytes(), 10));

			// SHA256-HMAC and Base64 encoding for "sig" string
			// String sicretKey = "MTI85XVvZNO8W5Itw8hTzFHW29v2RrVI";
			String encoded64sig = null;
			try {
				SecretKey signingKey = new SecretKeySpec(localKey.getBytes(),
						"HMACSHA256");
				Mac mac = Mac.getInstance("HMACSHA256");
				mac.init(signingKey);
				byte[] digest = mac.doFinal(encoded64data.getBytes()); // output
																		// of
																		// sha256
				encoded64sig = str_replace(Base64.encodeToString(digest, 10)); // using
																				// it
																				// as
																				// an
																				// input
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (InvalidKeyException e) {
				e.printStackTrace();
			}

			// write parameters
			writer.write("data=" + encoded64data + "&sig=" + encoded64sig);
			writer.flush();

			// Get the response
			StringBuffer answer = new StringBuffer();
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					conn.getInputStream()));
			String line;
			while ((line = reader.readLine()) != null) {
				answer.append(line);
			}
			writer.close();
			reader.close();

			// Return the response
			if(answer!=null){
				answerJSON = new JSONObject(answer.toString());
				result = answerJSON.getJSONObject("task").isNull("result");
				Log.v(tag,"Player AddPlayer: "+answerJSON.toString());
				if(!result){
					if(answerJSON.getString("status").equals("exist")){
						if (!answerJSON.getJSONObject("task").getString("result").equals("notfound")){
							setAddAnswer(answer);
							setAddProblems(true);

						}
					}
				}
			}else{
				setAddProblems(false);
			}

		} catch (MalformedURLException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void setAddAnswer(StringBuffer answer2) {
		this.addPlayerAnswer = answer2;
		
	}

	private void setAddProblems(boolean b) {
		this.addProblems = b;
		
	}

	// Connecting to player stats
	public boolean ConnectingToStats(int player) {
		HttpClient client = new DefaultHttpClient();
		HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); // Timeout
																				// Limit
		HttpResponse response = null;
		if (player == 1) {
			post = new HttpPost(GetUrl(getPlayerPlatform()));
		} else {
			post = new HttpPost(GetUrl(getPlayer2platform()));
		}
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		if (player == 1) {
			params.add(new BasicNameValuePair("player", getPlayerName()));
		} else if (player == 2) {
			params.add(new BasicNameValuePair("player", getPlayer2name()));
		}
		params.add(new BasicNameValuePair("opt", "all"));
		UrlEncodedFormEntity ent = null;
		try {
			ent = new UrlEncodedFormEntity(params, HTTP.UTF_8);
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		post.setEntity(ent);
		try {
			response = client.execute(post);
			statusLine = response.getStatusLine();
			entity = response.getEntity();
		} catch (ClientProtocolException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
		    if(entity == null){
		        connectionprobelem = true;
		    }else{
		        content = entity.getContent();
		    }
		} catch (IllegalStateException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
		    if(!connectionprobelem){
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200 || statusCode == 400 || statusCode == 500) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(content, "iso-8859-1"), 8);
				StringBuilder sb = new StringBuilder();
				String line = null;
				while ((line = reader.readLine()) != null) {
					sb.append(line + "n");
				}
				content.close();
				if (sb.toString().equals(null)) {
					connectionprobelem = true;
				} else {
					if (player == 1) {
						setJsonString(sb.toString());
					} else if (player == 2) {
						setJsonString2(sb.toString());
					}
				}
			} else {
				connectionprobelem = true;
			}
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
		weAreGood = true;
		return connectionprobelem;
	}

	protected StringBuffer AddPlayerToMOHstats(String playerName,
			String platform, String appIdent, String timestamp) {

		// Build parameter string
		// String data =
		// "time="+timestamp+"&ident="+appIdent+"&player="+playerName;
		String data;
		String sig;

		try {

			// Send the request
			URL url = new URL("http://api.mohwstats.com/" + platform
					+ "/playerlookup/");
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			OutputStreamWriter writer = new OutputStreamWriter(
					conn.getOutputStream());

			// JSON and Base64 encoding for "data" string
			JSONObject json = new JSONObject();
			try {
				json.put("time", timestamp);
				json.put("ident", appIdent);
				json.put("player", playerName);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			data = json.toString();
			String encoded64data = str_replace(Base64.encodeToString(
					data.getBytes(), 10));

			// SHA256-HMAC and Base64 encoding for "sig" string
			// String sicretKey = "MTI85XVvZNO8W5Itw8hTzFHW29v2RrVI";
			String encoded64sig = null;
			try {
				SecretKey signingKey = new SecretKeySpec(localKey.getBytes(),
						"HMACSHA256");
				Mac mac = Mac.getInstance("HMACSHA256");
				mac.init(signingKey);
				byte[] digest = mac.doFinal(encoded64data.getBytes()); // output
																		// of
																		// sha256
				encoded64sig = str_replace(Base64.encodeToString(digest, 10)); // using
																				// it
																				// as
																				// an
																				// input
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (InvalidKeyException e) {
				e.printStackTrace();
			}

			// write parameters
			writer.write("data=" + encoded64data + "&sig=" + encoded64sig);
			writer.flush();

			// Get the response
			StringBuffer answer = new StringBuffer();
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					conn.getInputStream()));
			String line;
			while ((line = reader.readLine()) != null) {
				answer.append(line);
			}
			writer.close();
			reader.close();

			// Return the response
			return answer;

		} catch (MalformedURLException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		return null;
	}

	public static String str_replace(String text) {
		text = text.replace('+', '-');
		text = text.replace('/', '_');
		text = text.replace("=", "");
		return text;
	}

	private String GetUrl(String platform) {
		sendURL = url.concat(platform);
		sendURL = sendURL.concat("/");
		sendURL = sendURL.concat("player");
		sendURL = sendURL.concat("/");
		return sendURL;
	}

	private String GetUrlForUpdate(String platform) {
		sendURL = url.concat(platform);
		sendURL = sendURL.concat("/");
		sendURL = sendURL.concat("playerupdate");
		sendURL = sendURL.concat("/");
		return sendURL;
	}

	public static int byteToUnsignedInt(byte b) {
		return 0x00 << 24 | b & 0xff;
	}

	public String hash_hmac(String type, String value, String key) {
		try {
			javax.crypto.Mac mac = javax.crypto.Mac.getInstance(type);
			javax.crypto.spec.SecretKeySpec secret = new javax.crypto.spec.SecretKeySpec(
					key.getBytes(), type);
			mac.init(secret);
			byte[] digest = mac.doFinal(value.getBytes());
			StringBuilder sb = new StringBuilder(digest.length * 2);
			String s;
			for (byte b : digest) {
				s = Integer.toHexString(byteToUnsignedInt(b));
				if (s.length() == 1)
					sb.append('0');
				sb.append(s);
			}
			return sb.toString();
		} catch (Exception e) {
		}
		return "";
	}

	private void setPlayer2Platform(String platform) {
		this.player2platform = platform;

	}

	private void setPlayer2Name(String playername3) {
		this.player2name = playername3;

	}

	private StringBuffer getAppKey() {
		String data;
		String sig;

		try {
			String key = generateKey();
			// Send the request
			URL url = new URL("http://api.mohwstats.com/global/getkey/");
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			OutputStreamWriter writer = new OutputStreamWriter(
					conn.getOutputStream());

			// JSON and Base64 encoding for "data" string
			JSONObject json = new JSONObject();
			try {
				json.put("time", "" + now());
				json.put("ident", appidentification);
				json.put("clientident", key);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			data = json.toString();
			String encoded64data = str_replace(Base64.encodeToString(
					data.getBytes(), 10));

			// SHA256-HMAC and Base64 encoding for "sig" string
			String encoded64sig = null;
			try {
				SecretKey signingKey = new SecretKeySpec(sicretKey.getBytes(),
						"HMACSHA256");
				Mac mac = Mac.getInstance("HMACSHA256");
				mac.init(signingKey);
				byte[] digest = mac.doFinal(encoded64data.getBytes()); // output
																		// of
																		// sha256
				encoded64sig = str_replace(Base64.encodeToString(digest, 10)); // using
																				// it
																				// as
																				// an
																				// input
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (InvalidKeyException e) {
				e.printStackTrace();
			}

			// write parameters
			writer.write("data=" + encoded64data + "&sig=" + encoded64sig);
			writer.flush();

			// Get the response
			StringBuffer answer = new StringBuffer();
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					conn.getInputStream()));
			String line;
			while ((line = reader.readLine()) != null) {
				answer.append(line);
			}
			writer.close();
			reader.close();

			// Return the response
			return answer;

		} catch (MalformedURLException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		return null;
	}

	private void CheckAppKey() {
		db = new Database(this);
		db.open();
		c = db.getKey();
		if (c != null) {
			if (c.moveToFirst()) {
				keyIsPressent = true;
				localIdent = c.getString(c.getColumnIndex("key"));
				localKey = c.getString(c.getColumnIndex("hesh"));
			}
		}
		c.close();
		db.close();
		if (!keyIsPressent) {
			something = setupAppKey();
			if (something == null) {
			} else {
				jsonKeyString = something.toString();
				try {
					jsonKey = new JSONObject(jsonKeyString);
					status = jsonKey.getString("status");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (status.equals("ok")) {
					db.open();
					try {
						db.insertKey(jsonKey.getString("ident"),
								jsonKey.getString("key"));
						localIdent = jsonKey.getString("ident");
						localKey = jsonKey.getString("key");
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					db.close();
				} else {
					something = getAppKey();
					if (something == null) {
					} else {
						jsonKeyString = something.toString();
						try {
							jsonKey = new JSONObject(jsonKeyString);
							status = jsonKey.getString("status");
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if (status.equals("ok")) {
							db.open();
							try {
								db.insertKey(jsonKey.getString("ident"),
										jsonKey.getString("key"));
								localIdent = jsonKey.getString("ident");
								localKey = jsonKey.getString("key");
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							db.close();
						} else {
							localIdent = appidentification;
							localKey = sicretKey;
						}
					}
				}
			}
		}
	}

	private StringBuffer setupAppKey() {
		String data;
		String sig;

		try {
			String key = generateKey();
			// Send the request
			URL url = new URL("http://api.mohwstats.com/global/setupkey/");
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			OutputStreamWriter writer = new OutputStreamWriter(
					conn.getOutputStream());

			// JSON and Base64 encoding for "data" string
			JSONObject json = new JSONObject();
			try {
				json.put("time", "" + now());
				json.put("ident", appidentification);
				json.put("clientident", key);
				json.put("name", "New Client");
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			data = json.toString();
			String encoded64data = str_replace(Base64.encodeToString(
					data.getBytes(), 10));

			// SHA256-HMAC and Base64 encoding for "sig" string
			String encoded64sig = null;
			try {
				SecretKey signingKey = new SecretKeySpec(sicretKey.getBytes(),
						"HMACSHA256");
				Mac mac = Mac.getInstance("HMACSHA256");
				mac.init(signingKey);
				byte[] digest = mac.doFinal(encoded64data.getBytes()); // output
																		// of
																		// sha256
				encoded64sig = str_replace(Base64.encodeToString(digest, 10)); // using
																				// it
																				// as
																				// an
																				// input
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (InvalidKeyException e) {
				e.printStackTrace();
			}

			// write parameters
			writer.write("data=" + encoded64data + "&sig=" + encoded64sig);
			writer.flush();

			// Get the response
			StringBuffer answer = new StringBuffer();
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					conn.getInputStream()));
			String line;
			while ((line = reader.readLine()) != null) {
				answer.append(line);
			}
			writer.close();
			reader.close();

			// Return the response
			return answer;

		} catch (MalformedURLException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		return null;

	}

	// Generate unique key (10 chars) for device
	private String generateKey() {
		final TelephonyManager tm = (TelephonyManager) getBaseContext()
				.getSystemService(Context.TELEPHONY_SERVICE);

		final String tmDevice, tmSerial, androidId;
		tmDevice = "" + tm.getDeviceId();
		tmSerial = "" + tm.getSimSerialNumber();
		androidId = ""
				+ android.provider.Settings.Secure.getString(
						getContentResolver(),
						android.provider.Settings.Secure.ANDROID_ID);

		UUID deviceUuid = new UUID(androidId.hashCode(),
				((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
		String deviceId = deviceUuid.toString();
		return deviceId.substring(deviceId.length() - 10);

	}

	// Getting all data which we can for our player
	public boolean getData() {
		try {
			jsonObject = new JSONObject(jsonString);
			getStats(jsonObject);
			getNations(jsonObject);
			getKit(jsonObject);
			getWeapons(jsonObject);
			getRibons(jsonObject);
			getMedals(jsonObject);

			return true;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			return false;
		} catch (NullPointerException e) {
			return false;
		}
	}

	private void getMedals(JSONObject jsonObject3) {
		try {
			medalsJson = jsonObject3.getJSONObject("stats").getJSONObject(
					"medals");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void getRibons(JSONObject jsonObject3) {
		try {
			ribbonJson = jsonObject3.getJSONObject("stats").getJSONObject(
					"ribbons");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void getWeapons(JSONObject jsonObject3) {
		try {
			weaponJson = jsonObject3.getJSONObject("stats").getJSONObject(
					"weapons");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void getKit(JSONObject jsonObject3) {
		try {
			kitJson = jsonObject3.getJSONObject("stats").getJSONObject("kits");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void getNations(JSONObject jsonObject3) {
		try {
			nationsJson = jsonObject3.getJSONObject("stats").getJSONObject(
					"nations");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void setWeapons(JSONObject statsJson2) {
		try {
			weaponJson = statsJson2.getJSONObject("weapons");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void getStats(JSONObject jsonObject3) {
		try {
			statsJson = jsonObject3.getJSONObject("stats");
			rank = statsJson.getJSONObject("rank").getString("nr");
			rankimage = statsJson.getJSONObject("rank").getString("img_medium");
			score = statsJson.getJSONObject("scores").getInt("score");
			//playerFlag = statsJson.getString("country_img");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}

	}

	public void setConnectionProblems(boolean problem) {
		this.connectionprobelem = problem;
	}

	public boolean getConnectionProblems() {
		return connectionprobelem;
	}

	public void setState(int i) {
		this.state = i;
	}

	public int getState() {
		return this.state;
	}

	public void setPlayerPlatform(String playerPlatform) {
		this.playerPlatform = playerPlatform;
	}

	public String getPlayerPlatform() {
		return playerPlatform;
	}

	public String getPlayer2platform() {
		return player2platform;
	}

	public void setPlayer2platform(String player2platform) {
		this.player2platform = player2platform;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public String getPlayerName() {
		return playerName;
	}

	public String getPlayer2name() {
		return player2name;
	}

	public void setPlayer2name(String player2name) {
		this.player2name = player2name;
	}

	public String getJsonString() {
		return jsonString;
	}

	public void setJsonString(String jsonString) {
		this.jsonString = jsonString;
	}

	public String getJsonString2() {
		return jsonString2;
	}

	public void setJsonString2(String jsonString2) {
		this.jsonString2 = jsonString2;
	}

	public JSONObject getJsonObject2() {
		return jsonObject2;
	}

	public void setJsonObject2(JSONObject jsonObject2) {
		this.jsonObject2 = jsonObject2;
	}

	public JSONObject getWeapons() {
		// TODO Auto-generated method stub
		return weaponJson;
	}

	public String getPlayerRank() {
		return rank;
	}

	public String getPlayerRankImmage() {
		// TODO Auto-generated method stub
		return rankimage;
	}

	public int getPlayerScore() {
		// TODO Auto-generated method stub
		return score;
	}

	public int getPlayerPreviouseRankScore() {
		// TODO Auto-generated method stub
		return previouseScore;
	}

	public int getPlayerNextRankScore() {
		// TODO Auto-generated method stub
		return nextRankScore;
	}

	public String getPlayerFlag() {
		return playerFlag;
	}

	public String getCountryname() {
		// TODO Auto-generated method stub
		return playerCountry;
	}

	public String getPlayerKillStreek() {
		// TODO Auto-generated method stub
		return playerKillStreak;
	}

	public int getPlayerShootsFired() {
		// TODO Auto-generated method stub
		return playerShootsFired;
	}

	public int getPlayerHits() {
		// TODO Auto-generated method stub
		return playerHits;
	}

	public long getPlayerGameTime() {
		// TODO Auto-generated method stub
		return playerGameTime;
	}

	public long getPlayerGlobalKills() {
		// TODO Auto-generated method stub
		return playerKills;
	}

	public long getPlayerGlobalDeaths() {
		// TODO Auto-generated method stub
		return playerDeaths;
	}

	public long getPlayerGameWin() {
		// TODO Auto-generated method stub
		return playerGameWin;
	}

	public long getPlayerGameLose() {
		// TODO Auto-generated method stub
		return playerGameLose;
	}

	public String getPlayerWinStreak() {
		// TODO Auto-generated method stub
		return playerWinStreak;
	}

	public int getPlayerTokenSpent() {
		// TODO Auto-generated method stub
		return playerTokenSpent;
	}

	public int getTokenLeft() {
		// TODO Auto-generated method stub
		return playerTokenLeft;
	}

	public int getPlayerMeleeKills() {
		// TODO Auto-generated method stub
		return playerMelleKils;
	}

	public int getPlayerFirstPlace() {
		// TODO Auto-generated method stub
		return playerFirstPlace;
	}

	public int getPlayerSecondPlace() {
		// TODO Auto-generated method stub
		return playerSecondPlace;
	}

	public int getPlayerThirdPlace() {
		// TODO Auto-generated method stub
		return playerThirdPlace;
	}

	public int getPlayerTop3() {
		// TODO Auto-generated method stub
		return playerTop3;
	}

	public JSONObject getNationJSON() {
		return nationsJson;
	}

	public JSONObject getKitJSON() {
		return kitJson;
	}

	public JSONObject getWeaponJSON() {
		return weaponJson;
	}

	public JSONObject getRibbonsJSON() {
		return ribbonJson;
	}

	public JSONObject getMedalsJSON() {
		return medalsJson;
	}
	
	public JSONObject getJSON(){
		return jsonObject;
	}
}
