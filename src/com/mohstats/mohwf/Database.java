package com.mohstats.mohwf;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Database {
	public static final String KEY_ROWID = "id";
	public static final String KEY_NAME = "name";
	public static final String KEY_PLATFORM = "platform";
	public static final String KEY_RANK = "rank";
	public static final String KEY_RANKIMAGE = "rankimage";
	public static final String KEY_CURENTSCORE = "curentscore";
	public static final String KEY_NEXTSCORE = "nextscore";
	public static final String KEY_PREVIOUSESCORE = "previousescore";
	private static final String tag = "djevtic";
	private static final String DATABASE_NAME = "medalofhonor";
	private static final String DATABASE_TABLE = "players";
	private static final int DATABASE_VERSION = 3;
	private static final String DATABASE_CREATE =
	"create table players (id integer primary key autoincrement, "
	+ "name text not null, platform text not null, "
	+ "rank text not null, rankimage text not null, "
	+ "curentscore text not null, nextscore text not null, previousescore text not null);";
	private static final String DATABASE_KEY_TABLE = "appkey";
	private static final String KEY_TABLE_ID = "id";
	private static final String KEY_TABLE_KEY = "key";
	private static final String KEY_TABLE_HESH = "hesh";
	private static final String KEY_TABLE_CREATE = 
			"create table appkey (id integer primary key autoincrement,"
			+ "key text not null, hesh text not null);";
	private final Context context;
	private DatabaseHelper DBHelper;
	private SQLiteDatabase db;
	public Database(Context ctx)
	{
		this.context = ctx;
		DBHelper = new DatabaseHelper(context);
	}
	
	private static class DatabaseHelper extends SQLiteOpenHelper
	{
		DatabaseHelper(Context context)
		{
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}
		
		@Override
		public void onCreate(SQLiteDatabase db)
		{
			db.execSQL(DATABASE_CREATE);
			db.execSQL(KEY_TABLE_CREATE);
		}
	
		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion,int newVersion)
		{
			db.execSQL("DROP TABLE IF EXISTS players");
			db.execSQL("DROP TABLE IF EXISTS appkey");
			onCreate(db);
		}
	}
	
	//---opens the database---
	public Database open() throws SQLException
	{
		db = DBHelper.getWritableDatabase();
		return this;
	}
	
	//---closes the database---
	public void close()
	{
		DBHelper.close();
	}
	
	//---insert a Player into the database---
	public long insertPlayer(String name, String platform, String rankimage, String rank, String curentscore, String nextscore, String previousescore)
	{
		ContentValues initialValues = new ContentValues();
		initialValues.put(KEY_NAME, name);
		initialValues.put(KEY_PLATFORM, platform);
		initialValues.put(KEY_RANK, rank);
		initialValues.put(KEY_RANKIMAGE, rankimage);
		initialValues.put(KEY_CURENTSCORE, curentscore);
		initialValues.put(KEY_NEXTSCORE, nextscore);
		initialValues.put(KEY_PREVIOUSESCORE,previousescore);
		return db.insert(DATABASE_TABLE, null, initialValues);
	}
	
	//--insert key in database---
	public long insertKey(String key, String hesh){
		ContentValues initialValues = new ContentValues();
		initialValues.put(KEY_TABLE_KEY, key);
		initialValues.put(KEY_TABLE_HESH, hesh);
		return db.insert(DATABASE_KEY_TABLE, null, initialValues);
	}
	
	//--get key from database---
	//---retrieves a particular title---
		public Cursor getKey() throws SQLException
		{
			return db.query(DATABASE_KEY_TABLE, new String[] {
					KEY_TABLE_ID,
					KEY_TABLE_KEY,
					KEY_TABLE_HESH
					},
					null,
					null,
					null,
					null,
					null);
		}
	
	//---deletes a particular title---
	public boolean deletePlayer(long rowId)
	{
		return db.delete(DATABASE_TABLE, KEY_ROWID +
				"=" + rowId, null) > 0;
	}
	
	//---retrieves all the titles---
	public Cursor getAllPlayers()
	{
		return db.query(DATABASE_TABLE, new String[] {
				KEY_ROWID,
				KEY_NAME,
				KEY_PLATFORM,
				KEY_RANK,
				KEY_RANKIMAGE,
				KEY_CURENTSCORE,
				KEY_NEXTSCORE,
				KEY_PREVIOUSESCORE},
				null,
				null,
				null,
				null,
				null);
	}
	
	//---retrieves a particular title---
	public Cursor getPlayer(long rowId) throws SQLException
	{
	Cursor mCursor =
			db.query(true, DATABASE_TABLE, new String[] {
					KEY_ROWID,
					KEY_NAME,
					KEY_PLATFORM,
					KEY_RANK,
					KEY_RANKIMAGE,
					KEY_CURENTSCORE,
					KEY_NEXTSCORE,
					KEY_PREVIOUSESCORE
			},
			KEY_ROWID + "=" + rowId,
			null,
			null,
			null,
			null,
			null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}
	
	//---updates a title---
	public boolean updatePlayer(long rowId, String name, String platform, String rankimage, String rank, String curentscore, String nextscore, String previousescore)
	{
		ContentValues args = new ContentValues();
		args.put(KEY_NAME, name);
		args.put(KEY_PLATFORM, platform);
		args.put(KEY_RANK, rank);
		args.put(KEY_RANKIMAGE, rankimage);
		args.put(KEY_CURENTSCORE, curentscore);
		args.put(KEY_NEXTSCORE, nextscore);
		args.put(KEY_PREVIOUSESCORE, previousescore);
		return db.update(DATABASE_TABLE, args,KEY_ROWID + "=" + rowId, null) > 0;
	}

}
