package com.mohstats.mohwf;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;


public class About extends Activity{

	private ImageView bfstatimage;
	private ImageView oldguys;
	private Context context;

	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about);
        context = getApplicationContext();
        addListenerOnButton();
        addImageOnClickListener();
	}
	
	private void addImageOnClickListener() {
		bfstatimage = (ImageView) findViewById(R.id.bfstatimage);
		bfstatimage.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent urlIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://mohwstats.com//"));
				startActivity(urlIntent);
			}
 
		});
		oldguys = (ImageView) findViewById(R.id.bfstatimage);
		oldguys.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent urlIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.oldguys.eu/"));
				startActivity(urlIntent);
			}
 
		});
	}
	
	public void addListenerOnButton() {
		 
		Button emailButton = (Button) findViewById(R.id.email_button);
		Button marketButton = (Button) findViewById(R.id.market_button);
		Button donateButton = (Button) findViewById(R.id.donate_button);
 
		emailButton.setOnClickListener(new OnClickListener() {
 
			@Override
			public void onClick(View arg0) {
 
				Intent i = new Intent(Intent.ACTION_SEND);
				i.setType("message/rfc822");
				i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"gisseares@gmail.com"});
				i.putExtra(Intent.EXTRA_SUBJECT, "MOHWStats");
				i.putExtra(Intent.EXTRA_TEXT   , "Enter your text here");
				try {
				    startActivity(Intent.createChooser(i, "Send mail..."));
				} catch (android.content.ActivityNotFoundException ex) {
				    Toast.makeText(About.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
				}
 
			}
 
		});
		
		marketButton.setOnClickListener(new OnClickListener() {
			 
			@Override
			public void onClick(View arg0) {
 
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("market://details?id=com.mohstats.mohwf"));
				startActivity(intent);
 
			}
 
		});
		
		donateButton.setOnClickListener(new OnClickListener() {
			 
			@Override
			public void onClick(View arg0) {
 
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=N3D359RYJBY54"));
				startActivity(intent);
 
			}
 
		});
 
	}
	  
	@Override
    public void onBackPressed(){
		Intent i = new Intent(context, PlayerSelector.class);
		startActivity(i);
	}
}
