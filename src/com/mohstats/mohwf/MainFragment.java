package com.mohstats.mohwf;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

public class MainFragment extends FragmentActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide fragments for each of the
     * sections. We use a {@link android.support.v4.app.FragmentPagerAdapter} derivative, which will
     * keep every loaded fragment in memory. If this becomes too memory intensive, it may be best
     * to switch to a {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;
    
    Database db;
    Player player;
	
	private Cursor c;

	private String playerID;

	private boolean idweupdated;

	private String tag="djevtic";
	
	private ProgressDialog progress;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new ProgressDialog(MainFragment.this);
		progress = ProgressDialog.show(MainFragment.this, "", "Populating ...");
        // Create the adapter that will return a fragment for each of the three primary sections
        // of the app.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());


        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        player = Player.getInstance();
        databaseCheck();
        progress.dismiss();
    }

    private void databaseCheck() {
		boolean playerExist = false;
		db = new Database(this);
		db.open();
		long id = 0;
		c = db.getAllPlayers();
		if (c != null) {
			if (c.moveToFirst()) {
				do {
					if (player.getPlayerName().equals(
							c.getString(c.getColumnIndex("name")))) {
						playerExist = true;
						playerID = c.getString(c.getColumnIndex("id"));
					}
				} while (c.moveToNext());
			}
		}
		if (!playerExist) {
			id = db.insertPlayer(
					player.getPlayerName(),
					player.getPlayerPlatform(),
					player.getPlayerRankImmage(),
					player.getPlayerRank(),
					""
							+player.getPlayerScore()+"",
					""
							+ ((player.getPlayerNextRankScore() - player
									.getPlayerPreviouseRankScore()) / 2), "0");
		} else {
			idweupdated = db.updatePlayer(
					Long.parseLong(playerID),
					player.getPlayerName(),
					player.getPlayerPlatform(),
					player.getPlayerRankImmage(),
					player.getPlayerRank(),
					""
							+ player.getPlayerScore()+"",
					""
							+ ((player.getPlayerNextRankScore() - player
									.getPlayerPreviouseRankScore()) / 2), "0");
		}
		db.close();

	}
    


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to one of the primary
     * sections of the app.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
        	if(i == 0||i==6){
        		return PageFragment.newInstance(i);
        	}else{
        		return PageListFragment.newInstance(i);
        	}
        	
        }

        @Override
        public int getCount() {
            return 7;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0: return "Player Info";
                case 1: return "Units";
                case 2: return "Kits";
                case 3: return "Weapons";
                case 4: return "Ribbons";
                case 5: return "Medals";
                case 6: return "About";
            }
            return null;
        }
    }
    
    @Override
    public void onBackPressed(){
		Intent i = new Intent(getApplicationContext(), PlayerSelector.class);
		startActivity(i);
	}

}
