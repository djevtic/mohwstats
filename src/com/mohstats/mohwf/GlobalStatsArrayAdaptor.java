package com.mohstats.mohwf;

import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mohstats.mohwf.Database;
import com.mohstats.mohwf.Player;
import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.LoaderSettings;
import com.novoda.imageloader.core.LoaderSettings.SettingsBuilder;
import com.novoda.imageloader.core.loader.Loader;
import com.novoda.imageloader.core.model.ImageTag;
import com.novoda.imageloader.core.model.ImageTagFactory;

public class GlobalStatsArrayAdaptor extends ArrayAdapter<String> {
	
	private String TAG = "djevtic";

	private Context context;
	private String[] values;
	private Player player;
	private String statsName;
	private HashMap<String, String> weaponHash;
	private JSONObject weaponJSON;
	private Database db;
	private ImageManager imageManager;
	private ImageTagFactory imageTagFactory;
	private Loader imageLoader;

	private View rowView;
	private ImageView imageViewGlobalStatistic;
	private TextView textViewGlobalStatisticName, textViewGlobalStatisticData;
	private HashMap<String, String> meMap;
	private TextView playerName;
	private ImageView playerRankImage;
	private Cursor c;
	private JSONObject nationJson;
	private ImageView imageNationFlag;
	private TextView textNationName;
	private TextView textNationScore;
	private TextView textNationDeats;
	private TextView textNationMelleKills;
	private TextView textNationKills;
	private JSONObject kitJson;
	private ImageView imageKit;
	private TextView textKitTime;
	private TextView textKitName;
	private TextView textKitScore;
	private TextView textKitKills;
	private TextView textKitDeats;
	private TextView textKitUnlocks;
	private JSONObject weaponJson;
	private ImageView imageWeapon;
	private ImageView imageWeaponPin1;
	private ImageView imageWeaponPin2;
	private ImageView imageWeaponPin3;
	private TextView textWeaponTime;
	private TextView textWeaponName;
	private TextView textWeaponScore;
	private TextView textWeaponKills;
	private TextView textWeaponDeats;
	private TextView textWeaponHits;
	private TextView textWeaponShots;
	private TextView textWeaponPin1;
	private TextView textWeaponPin2;
	private TextView textWeaponPin3;
	private TextView textWeaponUnitName;
	private ImageView imageWeaponKit;
	private ImageView imageWeaponNation;
	private JSONObject ribbonJson;
	private ImageView imageRibbon;
	private TextView textRibbonName;
	private TextView textRibbonCount;
	private JSONObject medalsJson;
	private ImageView imageMedal;
	private TextView textMedalName;
	private ProgressBar progresMedal;
	private TextView textProgresMedal;
	private TextView textProgresMedal2;
	private TextView textProgresMedal3;
	private TextView textProgresMedal4;
	private TextView textProgresMedal5;
	private TextView textProgresMedal6;
	private TextView playerRank;
	private TextView playerProgress;

	public GlobalStatsArrayAdaptor(Context context, String[] values,
			String stats) {
		super(context, R.layout.global_stat_content, values);
		this.context = context;
		this.values = values;
		this.player = Player.getInstance();
		this.statsName = stats;
		this.weaponHash = weaponHashMap();
		this.weaponJSON = player.getWeapons();
		this.db = new Database(context);
		// Odavde poceto sa image JAR implementacijom
		LoaderSettings settings = new SettingsBuilder()
				.withDisconnectOnEveryCall(true).build(context);
		imageManager = new ImageManager(context, settings);
		imageTagFactory = new ImageTagFactory(context,
				R.drawable.oldguysscaledmenu);
		imageTagFactory.setErrorImageId(R.drawable.oldguysscaledmenu);
		imageLoader = imageManager.getLoader();
		// Ovde je kraj
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		String s = values[position];
		if (statsName.equals("playerselector")) {
			rowView = inflater.inflate(R.layout.playerselectorseparatelayout,
					parent, false);
			playerRankImage = (ImageView) rowView
					.findViewById(R.id.playerRankImage);
			playerName = (TextView) rowView.findViewById(R.id.textName);
			playerRank = (TextView) rowView.findViewById(R.id.textRank);
			playerProgress = (TextView) rowView.findViewById(R.id.textScore);
			db.open();
			c = db.getAllPlayers();
			if (s != null) {
				if (c != null) {
					if (c.moveToFirst()) {
						do {
							if (c.getString(c.getColumnIndex("id")) != null) {
								if (s.equals(c.getString(c.getColumnIndex("id")))) {
									setImage(playerRankImage, c.getString(c
											.getColumnIndex("rankimage")));
									playerName.setText(c.getString(c
											.getColumnIndex("name")));
									playerRank.setText(c.getString(c.getColumnIndex("rank")));
									String score = c.getString(c.getColumnIndex("curentscore"));
									playerProgress.setText("Curent score: "+score);
								}
							}
						} while (c.moveToNext());
					}
				}
			}
			c.close();
			db.close();
		} else if (statsName.equals("nations")) {
			try {
				nationJson = player.getNationJSON().getJSONObject(s);
				rowView = inflater.inflate(R.layout.nationssinglelayout,
						parent, false);
				imageNationFlag = (ImageView) rowView
						.findViewById(R.id.nationFlagImage);
				textNationName = (TextView) rowView
						.findViewById(R.id.nationName);
				textNationScore = (TextView) rowView
						.findViewById(R.id.nationScore);
				textNationKills = (TextView) rowView
						.findViewById(R.id.nationKills);
				textNationDeats = (TextView) rowView
						.findViewById(R.id.nationDeaths);
				textNationMelleKills = (TextView) rowView
						.findViewById(R.id.nationMelleKills);
				Log.e(TAG,"nationJson.getString(unitImg): "+nationJson.getString("unitImg"));
				setImage(imageNationFlag, nationJson.getString("unitImg"));
				textNationScore.setText("" + nationJson.getLong("score"));
				textNationName.setText(nationJson.getString("name"));
				textNationKills.setText(nationJson.getString("kills"));
				textNationDeats.setText(nationJson.getString("deaths"));
				textNationMelleKills
						.setText(nationJson.getString("meleekills"));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NullPointerException e) {
				e.printStackTrace();
			}

		} else if (statsName.equals("kit")) {
			try {
				kitJson = player.getKitJSON().getJSONObject(s);

				rowView = inflater.inflate(R.layout.kitsinglelayout, parent,
						false);
				imageKit = (ImageView) rowView.findViewById(R.id.kitImage);
				textKitTime = (TextView) rowView.findViewById(R.id.kitTime);
				textKitName = (TextView) rowView.findViewById(R.id.kitName);
				textKitScore = (TextView) rowView.findViewById(R.id.kitScore);
				textKitKills = (TextView) rowView.findViewById(R.id.kitKills);
				textKitDeats = (TextView) rowView.findViewById(R.id.kitDeaths);
				textKitUnlocks = (TextView) rowView
						.findViewById(R.id.kitUnlocks);

				setImage(imageKit, kitJson.getString("img"));
				textKitTime.setText(getTime(kitJson.getLong("time")));
				textKitName.setText(kitJson.getString("name"));
				textKitScore.setText(kitJson.getString("totalscore"));
				textKitKills.setText(kitJson.getString("kills"));
				textKitDeats.setText(kitJson.getString("deaths"));
				textKitUnlocks.setText(kitJson.getString("unlocks"));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (statsName.equals("weapons")) {
			try {
				weaponJson = player.getWeaponJSON().getJSONObject(s);

				rowView = inflater.inflate(R.layout.weaponsinglelayout, parent,
						false);
				imageWeapon = (ImageView) rowView
						.findViewById(R.id.weaponImage);
				imageWeaponPin1 = (ImageView) rowView
						.findViewById(R.id.weaponPin1);
				imageWeaponPin2 = (ImageView) rowView
						.findViewById(R.id.weaponPin2);
				imageWeaponPin3 = (ImageView) rowView
						.findViewById(R.id.weaponPin3);
				textWeaponPin1 = (TextView) rowView
						.findViewById(R.id.weaponPin1txt);
				textWeaponPin2 = (TextView) rowView
						.findViewById(R.id.weaponPin2txt);
				textWeaponPin3 = (TextView) rowView
						.findViewById(R.id.weaponPin3txt);
				textWeaponUnitName = (TextView) rowView
						.findViewById(R.id.weaponUnit);
				imageWeaponKit = (ImageView) rowView
						.findViewById(R.id.weaponKitImage);
				imageWeaponNation = (ImageView) rowView
						.findViewById(R.id.weaponUnitImage);
				textWeaponTime = (TextView) rowView
						.findViewById(R.id.weaponTime);
				textWeaponName = (TextView) rowView
						.findViewById(R.id.weaponName);
				textWeaponScore = (TextView) rowView
						.findViewById(R.id.weaponScore);
				textWeaponKills = (TextView) rowView
						.findViewById(R.id.weaponKills);
				textWeaponDeats = (TextView) rowView
						.findViewById(R.id.weaponDeaths);
				textWeaponHits = (TextView) rowView
						.findViewById(R.id.weaponHits);
				textWeaponShots = (TextView) rowView
						.findViewById(R.id.weaponShots);

				setImage(imageWeapon, weaponJson.getString("img"));
				if (s.equals("whmT") || s.equals("whegM67")
						|| s.equals("mhegM87") || s.equals("mhegFlash")
						|| s.equals("Sa870") || s.equals("SaP226")
						|| s.equals("Sa45ct") || s.equals("Sa1911")) {
					// Nothing, Left for later
				} else {
					textWeaponUnitName
							.setText(weaponJson.getString("unitName"));
					setImage(imageWeaponKit, weaponJson.getString("kitImg"));
					setImage(imageWeaponNation, weaponJson.getString("unitImg"));
					setImage(imageWeaponPin1, weaponJson.getJSONArray("pins")
							.getJSONObject(0).getString("img"));
					setImage(imageWeaponPin2, weaponJson.getJSONArray("pins")
							.getJSONObject(1).getString("img"));
					setImage(imageWeaponPin3, weaponJson.getJSONArray("pins")
							.getJSONObject(2).getString("img"));
					textWeaponPin1.setText(getUnlockingString(
							weaponJson.getJSONArray("pins").getJSONObject(0)
									.getInt("curr"),
							weaponJson.getJSONArray("pins").getJSONObject(0)
									.getInt("needed")));
					textWeaponPin2.setText(getUnlockingString(
							weaponJson.getJSONArray("pins").getJSONObject(1)
									.getInt("curr"),
							weaponJson.getJSONArray("pins").getJSONObject(1)
									.getInt("needed")));
					textWeaponPin3.setText(getUnlockingString(
							weaponJson.getJSONArray("pins").getJSONObject(2)
									.getInt("curr"),
							weaponJson.getJSONArray("pins").getJSONObject(2)
									.getInt("needed")));
					if (weaponJson.getJSONArray("pins").getJSONObject(0)
							.getInt("curr") < weaponJson.getJSONArray("pins")
							.getJSONObject(0).getInt("needed")) {
						imageWeaponPin1.setAlpha(100);
					}
					if (weaponJson.getJSONArray("pins").getJSONObject(1)
							.getInt("curr") < weaponJson.getJSONArray("pins")
							.getJSONObject(1).getInt("needed")) {
						imageWeaponPin2.setAlpha(100);
					}
					if (weaponJson.getJSONArray("pins").getJSONObject(2)
							.getInt("curr") < weaponJson.getJSONArray("pins")
							.getJSONObject(2).getInt("needed")) {
						imageWeaponPin3.setAlpha(100);
					}
				}
				textWeaponTime.setText(getTime(weaponJson.getLong("time")));
				textWeaponName.setText(weaponJson.getString("name"));
				textWeaponScore.setText(weaponJson.getString("score"));
				textWeaponKills.setText(weaponJson.getString("kills"));
				textWeaponDeats.setText(weaponJson.getString("deaths"));
				textWeaponHits.setText(weaponJson.getString("hits"));
				textWeaponShots.setText(weaponJson.getString("shots"));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (statsName.equals("ribon")) {
			try {
				ribbonJson = player.getRibbonsJSON().getJSONObject(s);

				rowView = inflater.inflate(R.layout.ribbonssinglelayout,
						parent, false);
				imageRibbon = (ImageView) rowView
						.findViewById(R.id.ribbonImage);
				textRibbonName = (TextView) rowView
						.findViewById(R.id.ribbonName);
				textRibbonCount = (TextView) rowView
						.findViewById(R.id.ribbonCount);

				setImage(imageRibbon, ribbonJson.getString("img_s"));
				if (ribbonJson.getInt("count") == 0) {
					imageRibbon.setAlpha(100);
				}
				textRibbonName.setText(ribbonJson.getString("name"));
				textRibbonCount.setText(ribbonJson.getString("count"));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (statsName.equals("medal")) {
			try {
				medalsJson = player.getMedalsJSON().getJSONObject(s);

				if (s.equals("MLeMe")) {
					rowView = inflater.inflate(R.layout.medalsinglelayout5,
							parent, false);
				} else {
					rowView = inflater.inflate(R.layout.medalsinglelayout1,
							parent, false);
				}
				imageMedal = (ImageView) rowView.findViewById(R.id.medalImage);
				textMedalName = (TextView) rowView.findViewById(R.id.medalName);
				progresMedal = (ProgressBar) rowView
						.findViewById(R.id.progresMedal);
				textProgresMedal = (TextView) rowView
						.findViewById(R.id.medalProgresText);
				if (s.equals("MLeMe")) {
					textProgresMedal2 = (TextView) rowView
							.findViewById(R.id.medalProgresText2);
					textProgresMedal3 = (TextView) rowView
							.findViewById(R.id.medalProgresText3);
					textProgresMedal4 = (TextView) rowView
							.findViewById(R.id.medalProgresText4);
					textProgresMedal5 = (TextView) rowView
							.findViewById(R.id.medalProgresText5);
					textProgresMedal6 = (TextView) rowView
							.findViewById(R.id.medalProgresText6);
				}

				setImage(imageMedal, medalsJson.getString("img_s"));
				textMedalName.setText(medalsJson.getString("name"));
				if (s.equals("MLeMe")) {
					progresMedal.setMax(medalsJson.getJSONArray("criteria")
							.getJSONObject(0).getInt("needed")
							+ medalsJson.getJSONArray("criteria")
									.getJSONObject(1).getInt("needed")
							+ medalsJson.getJSONArray("criteria")
									.getJSONObject(2).getInt("needed")
							+ medalsJson.getJSONArray("criteria")
									.getJSONObject(3).getInt("needed")
							+ medalsJson.getJSONArray("criteria")
									.getJSONObject(4).getInt("needed")
							+ medalsJson.getJSONArray("criteria")
									.getJSONObject(5).getInt("needed"));
					progresMedal.setProgress(medalsJson
							.getJSONArray("criteria").getJSONObject(0)
							.getInt("curr")
							+ medalsJson.getJSONArray("criteria")
									.getJSONObject(1).getInt("curr")
							+ medalsJson.getJSONArray("criteria")
									.getJSONObject(2).getInt("curr")
							+ medalsJson.getJSONArray("criteria")
									.getJSONObject(3).getInt("curr")
							+ medalsJson.getJSONArray("criteria")
									.getJSONObject(4).getInt("curr")
							+ medalsJson.getJSONArray("criteria")
									.getJSONObject(5).getInt("curr"));
					textProgresMedal.setText(medalsJson
							.getJSONArray("criteria").getJSONObject(0)
							.getString("nname")
							+ " "
							+ medalsJson.getJSONArray("criteria")
									.getJSONObject(0).getInt("curr")
							+ "/"
							+ medalsJson.getJSONArray("criteria")
									.getJSONObject(0).getInt("needed"));
					textProgresMedal2.setText(medalsJson
							.getJSONArray("criteria").getJSONObject(1)
							.getString("nname")
							+ " "
							+ medalsJson.getJSONArray("criteria")
									.getJSONObject(1).getInt("curr")
							+ "/"
							+ medalsJson.getJSONArray("criteria")
									.getJSONObject(1).getInt("needed"));
					textProgresMedal3.setText(medalsJson
							.getJSONArray("criteria").getJSONObject(2)
							.getString("nname")
							+ " "
							+ medalsJson.getJSONArray("criteria")
									.getJSONObject(2).getInt("curr")
							+ "/"
							+ medalsJson.getJSONArray("criteria")
									.getJSONObject(2).getInt("needed"));
					textProgresMedal4.setText(medalsJson
							.getJSONArray("criteria").getJSONObject(3)
							.getString("nname")
							+ " "
							+ medalsJson.getJSONArray("criteria")
									.getJSONObject(3).getInt("curr")
							+ "/"
							+ medalsJson.getJSONArray("criteria")
									.getJSONObject(3).getInt("needed"));
					textProgresMedal5.setText(medalsJson
							.getJSONArray("criteria").getJSONObject(4)
							.getString("nname")
							+ " "
							+ medalsJson.getJSONArray("criteria")
									.getJSONObject(4).getInt("curr")
							+ "/"
							+ medalsJson.getJSONArray("criteria")
									.getJSONObject(4).getInt("needed"));
					textProgresMedal6.setText(medalsJson
							.getJSONArray("criteria").getJSONObject(5)
							.getString("nname")
							+ " "
							+ medalsJson.getJSONArray("criteria")
									.getJSONObject(5).getInt("curr")
							+ "/"
							+ medalsJson.getJSONArray("criteria")
									.getJSONObject(5).getInt("needed"));
				} else {
					progresMedal.setMax(medalsJson.getJSONArray("criteria")
							.getJSONObject(0).getInt("needed"));
					progresMedal.setProgress(medalsJson
							.getJSONArray("criteria").getJSONObject(0)
							.getInt("curr"));
					textProgresMedal.setText(medalsJson
							.getJSONArray("criteria").getJSONObject(0)
							.getString("nname")
							+ " "
							+ medalsJson.getJSONArray("criteria")
									.getJSONObject(0).getInt("curr")
							+ "/"
							+ medalsJson.getJSONArray("criteria")
									.getJSONObject(0).getInt("needed"));
				}
				if(progresMedal.getMax()!=progresMedal.getProgress()){
					imageMedal.setAlpha(100);
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {

			rowView = inflater.inflate(R.layout.global_stat_content, parent,
					false);
			imageViewGlobalStatistic = (ImageView) rowView
					.findViewById(R.id.playerStatContentImage);
			textViewGlobalStatisticName = (TextView) rowView
					.findViewById(R.id.textGlobalStatisticName);
			textViewGlobalStatisticData = (TextView) rowView
					.findViewById(R.id.textGlobalStatisticData);
			textViewGlobalStatisticName.setText(values[position]);
		}

		return rowView;
	}

	private String getUnlockingString(int curent, int needed) {
		if (curent < needed) {
			return "" + curent + "/" + needed;
		} else {
			return "" + needed + "/" + needed;
		}
	}

	public HashMap weaponHashMap() {

		meMap = new HashMap<String, String>();
		return meMap;
	}

	public String getTime(long timeLong) {
		String timeString = "";
		int seconds = (int) (timeLong) % 60;
		int minutes = (int) ((timeLong / 60) % 60);
		int hours = (int) ((timeLong / (60 * 60)) % 24);
		int days = (int) ((timeLong / (60 * 60 * 24)) % 365);
		if (days > 0) {
			timeString = timeString + days + "d ";
		}
		if (hours > 0) {
			timeString = timeString + hours + "h ";
		}
		timeString = timeString + minutes + "m " + seconds + "s ";
		return timeString;
	}

	private void setImage(ImageView imageView, String url) {
		String link = "https://dl.dropboxusercontent.com/u/1480498/mw/" + url;
		ImageTag tag = imageTagFactory.build(link);
		imageView.setTag(tag);
		imageLoader.load(imageView);
	}
}
