package com.mohstats.mohwf;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.ViewFlipper;

public class LoadingData extends Activity{
	private String tag = "djevtic";
	private String platform = "";
	private String name = "";
	Runnable runnable = null;
	Context context = null;
	Player player = null;
	LinearLayout loadingLayout;
	ViewFlipper page;
	Animation animFlipInForeward;
	Animation animFlipOutForeward;
	Animation animFlipInBackward;
	Animation animFlipOutBackward;
	private ProgressBar mProgress;
	private Database db;
		/** Called when the activity is first created. */
	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.loadingdata);
	        db = new Database(this);
	        context = getApplicationContext();
	        Bundle extras = getIntent().getExtras();
	        if (extras == null) {
	        		return;
	        		}
	        // Get data via the key
	        String value1 = extras.getString("name");
	        if (value1 != null) {
	        	name=value1;
	        }
	        String value2 = extras.getString("platform");
	        if(value2 != null){
	        	platform=value2;
	        }
	        player = Player.getInstance();
	        player.GetPlayer(value1, value2);
	        try {
	        	synchronized (this) {
	        		this.wait(2000);
	        	}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        if(player.getState()==1){
	        	player.setConnectionProblems(true);
	        }
	        player.setState(1);
	        //Toast creation
	        CharSequence connectionErrorMessage = "Problem with connection to statistic database, please try again later, looks like MOHWFStats are down :(";
	        int duration = Toast.LENGTH_LONG;
	        if(player.getConnectionProblems()){
	        	player.setConnectionProblems(false);
	        	Intent i = new Intent(context, PlayerSelect.class);
       		 	i.putExtra("error", false);
       		 	startActivity(i);
	        }else{
	        	GetData();
	        }
	    }
	    
	    private void GetData() {
	    	new Thread(new Runnable() {
	             public void run() {
	            	 while(!player.weAreGood){	                     
	                 }
	            	 boolean dataGood = player.getData();
	            	 if(dataGood){	            		 
	            		 Intent i = new Intent(context, MainFragment.class);
	            		 startActivity(i);
	            	 }else{
	            		 Intent i = new Intent(context, PlayerSelect.class);
	            		 i.putExtra("error", dataGood);
	            		 startActivity(i);
	            	 }
	             }
	         }).start();
		}
}
