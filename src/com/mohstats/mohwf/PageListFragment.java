package com.mohstats.mohwf;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.loader.Loader;
import com.novoda.imageloader.core.model.ImageTag;
import com.novoda.imageloader.core.model.ImageTagFactory;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;


public class PageListFragment extends ListFragment{
	
	
	public static PageListFragment newInstance(int page) {
		  
		PageListFragment pageListFragment = new PageListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("key", page);
        pageListFragment.setArguments(bundle);
        return pageListFragment;
    }


	private Player player;
	private String statname;
	private Context context;
	private ListView list;
	private ImageManager imageManager;
	private ImageTagFactory imageTagFactory;
	private Loader imageLoader;
	private String[] MenuStats;
	private GlobalStatsArrayAdaptor adapter;
	private JSONObject weaponJSON;
	private String weponCode;
	private JSONObject separateWeaponString;
	private String tag = "djevtic";
	private String[] sortByNumberOfRibons;
	private String ribonCode;
	private JSONObject ribonJSON;
	private JSONObject separateRibonString;
	private JSONObject medalJSON;
	private JSONObject separateMedalString;
	private String medalCode;

	private int site = 50666;
	private int zone = 64809;
	private LinearLayout linearLayout;
	
	@Override  
    public void onCreate(Bundle savedInstanceState) {  
        super.onCreate(savedInstanceState);  
    }  
	
    
   @Override  
   public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {  
       int page = getArguments().getInt("key");
       player = Player.getInstance();
       View view = null;
       	view = inflater.inflate(R.layout.statistics, container, false);
       	fillPage(page,view);
       	AdView mAdView = (AdView) view.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
       return view;  
   }



private void fillPage(int page, View view) {
	if(page==1){
		//Units
		MenuStats = getNationString();
		adapter = new GlobalStatsArrayAdaptor(getActivity(), MenuStats, "nations");
		setListAdapter(adapter);
	}else if(page==2){
		//Kits
		MenuStats = getKitString();
		adapter = new GlobalStatsArrayAdaptor(getActivity(), MenuStats, "kit");
		setListAdapter(adapter);
	}else if(page==3){
		//Weapons
		MenuStats = sortByKill();
		adapter = new GlobalStatsArrayAdaptor(getActivity(), MenuStats, "weapons");
		setListAdapter(adapter);
	}else if(page==4){
		//Ribons
		MenuStats = sortByNumberOfRibons();
		adapter = new GlobalStatsArrayAdaptor(getActivity(), MenuStats, "ribon");
		setListAdapter(adapter);
	}else if(page==5){
		//Medals
		MenuStats = sortMedals();//getMedalString();
		adapter = new GlobalStatsArrayAdaptor(getActivity(), MenuStats, "medal");
		setListAdapter(adapter);
	}
	
}
private String[] getMedalString() {
	String[] menuList = new String[] { "MmuOGA", "MLeMe", "MdeT1",
			"MvuSASR", "MlaT1", "Mso", "MSuSe", "MsnT1", "MoH", "MvuDELTA",
			"MhaT1", "Msg", "Mde", "MvuSPETS", "MFoCo", "MGoCo", "MvuUDT",
			"MEfCo", "MLeDi", "MApCo", "MAnVe", "MInCo", "MvuGROM", "MSu",
			"MSt", "MmuFSK", "MmuDELTA", "MHRCo", "MmuSASR", "MvuFSK",
			"MAcCo", "MsgT1", "Msn", "MBrSt", "MmuJTF2", "MCo", "Mla",
			"MvuKSK", "MCaCo", "MmuSPETS", "MVeCo", "MvuOGA", "Mha",
			"MvuJTF2", "MCoVe", "MHeOr", "MLeBl", "MLeFo", "MSiSt",
			"MCqCo", "MLeCq", "MLeDe", "MDiSe", "MmuUDT", "MhaCo", "MDiCo",
			"MvuSOG", "MLeFiTe", "MmuKSK", "MmuGROM", "MAc", "MDCG",
			"MmuSAS", "MvuSEAL", "MsoT1", "MBlCo", "MToPe", "MRe",
			"MmuSEAL", "MLeHR", "MDiInCo", "MmuSOG", "MFiTeCo", "MvuSAS" };
	return menuList;
}

private String[] getRibonsString() {
	String[] menuList = new String[] { "RPlUn", "RHoRuW", "RBoSqA",
			"RSide", "RBoSqD", "RTDMW", "RHoRuD", "RTDM", "RBoSqW",
			"RResc", "RDFT", "RMark", "RReta", "RBoSq", "RSeCoA", "RSeCoW",
			"RCoMi", "RNatPri", "RReco", "RFiTeK", "RFiAs", "RExEx",
			"RHoRuAS", "RKiCo", "RCoMiA", "RCoMiW", "RCQB", "RSeCo",
			"RHoRu", "RCoMiD", "RSeCoD" };
	return menuList;
}

private String[] getWeaponString() {
	String[] menuList = new String[] { "whmT", "WhaEBRJTF2", "WhaEBRSASR",
			"WhaEBRSPETS", "WhaHK416GROM", "WhaHK416SFOD", "WhaHK416SEAL",
			"WhaM4SAS", "WhaM4UDT", "WhaM4OGA", "WhaG3KSK", "WhaG3FSK",
			"WhaG3SOG", "WsgPKPSPETS", "WsgM240BSEAL", "WsgM240BSFOD",
			"WsgM240BSAS", "WsgM240BFSK", "WsgMG4KSK", "WsgMG4SASR",
			"WsgM249GROM", "WsgM249SOG", "WsgM249OGA", "WsgM249UDT",
			"WsgM249JFT2", "WsoAKbullSPETS", "WsoAKbullGROM",
			"WsoMk18JTF2", "WsoMk18SFOD", "WsoMk18SAS", "WsoMk18SOG",
			"WsoG36CFSK", "WsoG36CSASR", "WsoG36CKSK", "WsoMP7OGA",
			"WsoMP7SEAL", "WsoMP7UDT", "WlaOBR556KSK", "WlaOBR556SFOD",
			"WlaOBR556JTF2", "WlaOBR556UDT", "WlaOBR556SEAL", "WlaAK5SOG",
			"WlaAK103SPETS", "WlaAK103OGA", "WlaAK103GROM", "WlaAUGFSK",
			"WlaAUGSAS", "WlaAUGSASR", "WsnSC5SAS", "WsnSC5FSK",
			"WsnSC5OGA", "WsnTAC300SEAL", "WsnTAC300UDT", "WsnTAC300SPETS",
			"WsnTAC300SOG", "WsnOBR762SASR", "WsnOBR762SFOD",
			"WsnTAC50JTF2", "WsnTAC50GROM", "WsnTAC50KSK", "WdeAA12SASR",
			"WdeAA12SEAL", "WdeAA12SOG", "WdeHK416CJTF2", "WdeHK416CKSK",
			"WdeHK416CSAS", "WdeHK416CGROM", "WdeSCARFSK", "WdeSCAROGA",
			"WdeSCARSFOD", "WdeSuchkaUDT", "WdeSuchkaSPETZ", "whegM67",
			"mhegM87", "mhegFlash", "Sa870", "SaP226", "Sa45ct", "Sa1911" };
	return menuList;
}

private String[] getKitString() {
	String[] menuList = new String[] { "was", "la", "hv", "de", "sn", "sc" };
	return menuList;
}

private String[] getNationString() {
	String[] menuList = new String[] { "SEAL", "SOG", "UDT", "OGA", "JTF2",
			"SAS", "SASR", "SPET", "GROM", "FSK", "KSK", "DELTA" };
	return menuList;
}

private void setImage(ImageView imageView, String url) {
	String link = "https://dl.dropboxusercontent.com/u/1480498/mw/" + url;
	ImageTag tag = imageTagFactory.build(link);
	imageView.setTag(tag);
	imageLoader.load(imageView);
}

private String[] sortByKill() {
	String[] weaponString = getWeaponString();
	String[][] weaponKillsArray = new String [weaponString.length][2];
	for(int i = 0; weaponString.length>i;i++){
		weponCode = weaponString[i];
		try {
			weaponJSON = player.getWeapons();
			separateWeaponString = weaponJSON.getJSONObject(weponCode);
			weaponKillsArray[i][1] = separateWeaponString.getString("kills");
			weaponKillsArray[i][0] = weaponString[i];
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	String[][] temp = new String[1][2];
	for(int counter=0; weaponString.length>counter; counter++) { //Loop once for each element in the array.
            for (int i = 0; i < weaponKillsArray.length - 1; i++){
        		int min = i;
        		for (int  j = i + 1; j < weaponKillsArray.length; j++){
        			if (Long.parseLong(weaponKillsArray[j][1]) > Long.parseLong(weaponKillsArray[min][1])){
        				min = j;
        			}
        		}
        		if (i != min){ 
        			temp[0][0] = weaponKillsArray[i][0];
        			temp[0][1] = weaponKillsArray[i][1];
        			//int tmp = data[i];
        			weaponKillsArray[i][0]=weaponKillsArray[min][0];
        			weaponKillsArray[i][1]=weaponKillsArray[min][1];
        			//data[i] = data[min];
        			weaponKillsArray[min][0]=temp[0][0];
        			weaponKillsArray[min][1]=temp[0][1];
        			//data[min] = tmp;
    	      }
        	}
        }
	String[] arrayToReturn= new String[weaponKillsArray.length];
	for(int i = 0; weaponKillsArray.length>i;i++){
		arrayToReturn[i]=weaponKillsArray[i][0];
	}
	return arrayToReturn;
}

private String[] sortByNumberOfRibons() {
	String[] ribonString = getRibonsString();
	String[][] ribonNumberArray = new String [ribonString.length][2];
	for(int i = 0; ribonString.length>i;i++){
		ribonCode = ribonString[i];
		try {
			ribonJSON = player.getRibbonsJSON();
			separateRibonString = ribonJSON.getJSONObject(ribonCode);
			ribonNumberArray[i][1] = separateRibonString.getString("count");
			ribonNumberArray[i][0] = ribonString[i];
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	String[][] temp = new String[1][2];
	for(int counter=0; ribonString.length>counter; counter++) { //Loop once for each element in the array.
            for (int i = 0; i < ribonNumberArray.length - 1; i++){
        		int min = i;
        		for (int  j = i + 1; j < ribonNumberArray.length; j++){
        			if (Long.parseLong(ribonNumberArray[j][1]) > Long.parseLong(ribonNumberArray[min][1])){
        				min = j;
        			}
        		}
        		if (i != min){ 
        			temp[0][0] = ribonNumberArray[i][0];
        			temp[0][1] = ribonNumberArray[i][1];
        			//int tmp = data[i];
        			ribonNumberArray[i][0]=ribonNumberArray[min][0];
        			ribonNumberArray[i][1]=ribonNumberArray[min][1];
        			//data[i] = data[min];
        			ribonNumberArray[min][0]=temp[0][0];
        			ribonNumberArray[min][1]=temp[0][1];
        			//data[min] = tmp;
    	      }
        	}
        }
	String[] arrayToReturn= new String[ribonNumberArray.length];
	for(int i = 0; ribonNumberArray.length>i;i++){
		arrayToReturn[i]=ribonNumberArray[i][0];
	}
	return arrayToReturn;
}

private String[] sortMedals() {
	String[] medalString = getMedalString();
	String[][] medalNumberArray = new String [medalString.length][2];
	for(int i = 0; medalString.length>i;i++){
		medalCode = medalString[i];
		try {
			medalJSON = player.getMedalsJSON();
			separateMedalString = medalJSON.getJSONObject(medalCode);
			if(separateMedalString.equals("MLeMe")){
				medalNumberArray[i][1] = ""+(((separateMedalString.getJSONArray("criteria").getJSONObject(0).getLong("curr")
										+separateMedalString.getJSONArray("criteria").getJSONObject(1).getLong("curr")
										+separateMedalString.getJSONArray("criteria").getJSONObject(2).getLong("curr")
										+separateMedalString.getJSONArray("criteria").getJSONObject(3).getLong("curr")
										+separateMedalString.getJSONArray("criteria").getJSONObject(4).getLong("curr")
										+separateMedalString.getJSONArray("criteria").getJSONObject(5).getLong("curr"))*100)/(separateMedalString.getJSONArray("criteria").getJSONObject(0).getLong("needed")
												+separateMedalString.getJSONArray("criteria").getJSONObject(1).getLong("needed")
												+separateMedalString.getJSONArray("criteria").getJSONObject(2).getLong("needed")
												+separateMedalString.getJSONArray("criteria").getJSONObject(3).getLong("needed")
												+separateMedalString.getJSONArray("criteria").getJSONObject(4).getLong("needed")
												+separateMedalString.getJSONArray("criteria").getJSONObject(5).getLong("needed")));
			}else{
				medalNumberArray[i][1]= ""+((separateMedalString.getJSONArray("criteria").getJSONObject(0).getLong("curr")*100)
						/separateMedalString.getJSONArray("criteria").getJSONObject(0).getLong("needed"));
			}
			medalNumberArray[i][0] = medalString[i];
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	String[][] temp = new String[1][2];
	for(int counter=0; medalString.length>counter; counter++) { //Loop once for each element in the array.
            for (int i = 0; i < medalNumberArray.length - 1; i++){
        		int min = i;
        		for (int  j = i + 1; j < medalNumberArray.length; j++){
        			if (Long.parseLong(medalNumberArray[j][1]) > Long.parseLong(medalNumberArray[min][1])){
        				min = j;
        			}
        		}
        		if (i != min){ 
        			temp[0][0] = medalNumberArray[i][0];
        			temp[0][1] = medalNumberArray[i][1];
        			//int tmp = data[i];
        			medalNumberArray[i][0]=medalNumberArray[min][0];
        			medalNumberArray[i][1]=medalNumberArray[min][1];
        			//data[i] = data[min];
        			medalNumberArray[min][0]=temp[0][0];
        			medalNumberArray[min][1]=temp[0][1];
        			//data[min] = tmp;
    	      }
        	}
        }
	String[] arrayToReturn= new String[medalNumberArray.length];
	for(int i = 0; medalNumberArray.length>i;i++){
		arrayToReturn[i]=medalNumberArray[i][0];
	}
	return arrayToReturn;
}
}
